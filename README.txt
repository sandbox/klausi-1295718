
--------------------------------------------------------------------------------
                             Decimal Vote
--------------------------------------------------------------------------------

Maintainers:
 * Klaus Purer (klausi), klausi@klau.si

The Decimal Vote module provides a voting field type for arbitrary numeric
votes. It depends on Voting API:

http://drupal.org/project/votingapi


TODO:
* Improve theming support
* Add min/max ranges vor vote values
* Add selection for integer or decimal numbers
  